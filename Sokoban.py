import tkinter
import time

''''
Character map:

    1 = background
    o = player
    + = wall
    # = box
    T = target

'''''


class Player:
    def __init__(self, x, y):
        self.size = 30
        self.x = x
        self.y = y
        self.playerimage1 = tkinter.PhotoImage(file='img/pushr.png')
        self.playerimage2 = tkinter.PhotoImage(file='img/snail_r.png')

    def __repr__(self):
        return "Player's start position: " + "x = " + str(self.x) + " y = " + str(self.y)

    def draw(self, c, image):
        self.image_state = image
        self.canvas = c
        a = self.x * self.size
        b = self.y * self.size
        if self.image_state == 0:
            self.id = c.create_image(a + 15, b + 15, image=self.playerimage1)
        else:
            self.id = c.create_image(a + 15, b + 15, image=self.playerimage2)

    def move(self, xx, yy):
        self.x += xx
        self.y += yy
        self.canvas.move(self.id, xx * self.size, yy * self.size)


class Box:
    def __init__(self, x, y):
        self.size = 30
        self.x = x
        self.y = y
        self.boximage = tkinter.PhotoImage(file='img/box.png')
        self.boxcontrol = False

    def __repr__(self):
        return "Box position: " + "x = " + str(self.x) + " y = " + str(self.y)

    def draw(self, c):
        self.canvas = c
        a = self.x * self.size
        b = self.y * self.size
        self.id = c.create_image(a + 15, b + 15, image=self.boximage)

    def move(self, xx, yy):
        self.x += xx
        self.y += yy
        self.canvas.move(self.id, xx * self.size, yy * self.size)
        self.boxcontrol = True

    def send(self):
        return self.boxcontrol


class Animation2:
    def __init__(self, obr, x, y, canvas):
        self.canvas = canvas
        self.x, self.y = x, y
        self.obr = obr
        self.faza = 0
        self.id = self.canvas.create_image(self.x, self.y, image=self.obr[self.faza])

    def posun(self):
        self.faza = (self.faza + 1) % len(self.obr)
        self.canvas.itemconfig(self.id, image=self.obr[self.faza])


class Game:
    def __init__(self, file, image, steps):
        self.image_state = image
        self.size = 30
        self.canvas = tkinter.Canvas(width=self.size * 19, height=self.size * 22)
        self.pole = []
        self.box = []
        self.line_count = 0
        self.column_count = 0
        self.target = []
        self.map = file
        self.main = tkinter.PhotoImage(file='img/bc2.png')
        self.wallimage = tkinter.PhotoImage(file='img/stone.png')
        self.backimage = tkinter.PhotoImage(file='img/grass.png')
        self.canvas.bind('<Button-1>', self.back)
        self.playerimage1 = tkinter.PhotoImage(file='img/pushr.png')
        self.playerimage2 = tkinter.PhotoImage(file='img/snail_r.png')
        self.changedimage1 = tkinter.PhotoImage(file='img/pushl.png')
        self.changedimage2 = tkinter.PhotoImage(file='img/snail_l.png')
        self.end = False
        self.record = 0
        self.record_pole = []
        self.oldx = 0
        self.oldy = 0
        self.olda = 0
        self.oldb = 0
        self.recieve = False
        self.boxindex = 0
        self.limiter = 0
        self.stepcounter = steps

        self.steppole = []

        try:
            with open(file, 'r') as subor:
                for riadok in subor:
                    riadok = riadok.split()
                    self.pole.append(riadok)
                    print("original ==",self.pole)

            with open('records.txt', 'r+') as numbers:
                for records in numbers:
                    records = int(records.strip())
                    self.record_pole.append(records)

            with open('numberofsteps.txt', 'r') as subor:
                for riadok in subor:
                    riadok = int(riadok.strip())
                    self.steppole.append(riadok)


        except:
            print("No file")

        for i in range(len(self.pole)):
            for j in range(len(self.pole[i])):
                if self.pole[i][j] == "o":
                    self.object = Player(j, i)
                    self.pole[i][j] = "1"
                if self.pole[i][j] == "#":
                    self.box.append(Box(j, i))
                    self.pole[i][j] = "1"
        

        self.canvas.create_image(300, 330, image=self.main)
        self.draw_board(self.canvas)

        self.canvas.create_text(120, 600, text="Press 'R' to restart!", fill="white", font="arial 16 bold")
        self.info = self.canvas.create_text(120, 635, text="Number of steps: " + str(self.stepcounter), fill="white",
                                            font="arial 16 bold")

        self.back2 = self.canvas.create_rectangle(30, 550, 200, 580, state="hidden")
        self.canvas.create_text(115, 565, text="Back to menu", fill="red", font="arial 14 bold", activefill="white")

        self.recordtext = self.canvas.create_text(400, 565, text="Best score: " + str(self.record), fill="yellow",
                                                  font="arial 14 bold")

        self.canvas.create_text(400, 600, text="Press 'B' to step one back!", fill="white", font="arial 16 bold")

        if self.map == 'Board_easy.txt':
            self.record = self.record_pole[0]
            self.canvas.itemconfig(self.recordtext, text="Best score: " + str(self.record))
        elif self.map == 'Board_medium.txt':
            self.record = self.record_pole[1]
            self.canvas.itemconfig(self.recordtext, text="Best score: " + str(self.record))
        elif self.map == 'Board_hard.txt':
            self.record = self.record_pole[2]
            self.canvas.itemconfig(self.recordtext, text="Best score: " + str(self.record))

        self.canvas.bind_all('<Right>', self.right)
        self.canvas.bind_all('<Left>', self.left)
        self.canvas.bind_all('<Down>', self.down)
        self.canvas.bind_all('<Up>', self.up)
        self.canvas.bind_all('<r>', self.restart2)
        self.canvas.bind_all('<b>', self.step_back)

        self.anim = []
        for i in range(16):
            self.anim.append(tkinter.PhotoImage(file='anim/' + str(i) + '.png'))

        self.polee = []

        self.timer()
        self.canvas.pack()

    def timer(self):
        for a in self.polee:
            a.posun()
        self.canvas.after(200, self.timer)

    def draw_board(self, c):
        for i in range(len(self.pole)):
            self.line_count += 1
            for j in range(len(self.pole[i])):
                self.column_count += 1
                x = j * self.size
                y = i * self.size
                if self.pole[i][j] == "1":
                    self.canvas.create_image(x + 15, y + 15, image=self.backimage)
                elif self.pole[i][j] == "+":
                    self.canvas.create_image(x + 15, y + 15, image=self.wallimage)
                elif self.pole[i][j] == "T":
                    self.target.append((j, i))
                    self.canvas.create_rectangle(x, y, x + self.size, y + self.size, fill="blue")

        self.object.draw(c, self.image_state)

        for self.element in self.box:
            self.element.draw(c)

    def right(self, e):
        if self.image_state == 0:
            self.canvas.itemconfig(self.object.id, image=self.playerimage1)
        else:
            self.canvas.itemconfig(self.object.id, image=self.playerimage2)
        self.movement(1, 0)

    def left(self, e):
        if self.image_state == 0:
            self.canvas.itemconfig(self.object.id, image=self.changedimage1)
        else:
            self.canvas.itemconfig(self.object.id, image=self.changedimage2)
        self.movement(-1, 0)

    def down(self, e):
        self.movement(0, 1)

    def up(self, e):
        self.movement(0, -1)

    def isbox(self, dx, dy):
        for i in range(len(self.box)):
            if self.box[i].x == dx and self.box[i].y == dy:
                return i
        return -1

    def movement(self, dx, dy):
        j = self.object.x + dx
        k = self.object.y + dy
        if j < 0 or k < 0 or k >= self.column_count or self.pole[k][j] == "+":
            return
        bb = self.isbox(j, k)
        if bb >= 0:
            self.boxindex = self.isbox(j, k)
            jj = j + dx
            kk = k + dy
            if jj < 0 or kk < 0 or kk >= self.column_count or self.pole[kk][jj] == "+":
                print("Cannot move Box through walls")
                return
            if self.isbox(jj, kk) >= 0:
                print("Cannot move 2 Box!")
                return

            ###########################################################################################################
            self.box[bb].move(dx, dy)  # elmozditom a dobozt

            self.recieve = self.box[bb].send()
            # self.recieve - DEFAULTban - False = nem volt doboz mozditva
            # self.box[bb] = doboz indexe
            # .send visszakuldi a True -t hogy elmozdult a Box class bol
            # ha megmoydul egz doboz True lesz es ugy is marad

            self.olda = -(jj - j)
            self.oldb = -(kk - k)

            self.oldx = -(j - self.object.x)
            self.oldy = -(k - self.object.y)

            self.object.move(dx, dy)

            self.stepcounter += 1
            self.canvas.itemconfig(self.info, text="Number of steps: " + str(self.stepcounter))

            self.limiter = 0

        else:

            self.oldx = -(j - self.object.x)
            self.oldy = -(k - self.object.y)

            self.object.move(dx, dy)

            self.stepcounter += 1
            self.canvas.itemconfig(self.info, text="Number of steps: " + str(self.stepcounter))

            self.recieve = False

            self.limiter = 0

        for b in self.box:
            if self.pole[b.y][b.x] != 'T':
                return

        if self.map == 'Board_easy.txt':
            if self.stepcounter < self.record_pole[0]:
                self.record_pole[0] = self.stepcounter
        elif self.map == 'Board_medium.txt':
            if self.stepcounter < self.record_pole[1]:
                self.record_pole[1] = self.stepcounter
        elif self.map == 'Board_hard.txt':
            if self.stepcounter < self.record_pole[2]:
                self.record_pole[2] = self.stepcounter

        with open('records.txt', 'w') as output:
            for item in self.record_pole:
                output.write(str(item) + '\n')

        self.canvas.delete("all")
        self.end = True
        self.canvas.create_image(300, 330, image=self.main)

        self.polee.append(Animation2(self.anim, 300, 150, self.canvas))

        self.canvas.create_text(300, 220, text="YOU WIN!", fill='blue', font='arial 30 bold')

        self.canvas.create_text(300, 390,
                                text="Best scores: \n \nEasy: " + str(self.record_pole[0]) + '\n' + "Medium: " + str(
                                    self.record_pole[1]) + '\n' + "Hard: " + str(self.record_pole[2]), fill="red",
                                font="arial 14 bold")
        self.canvas.create_text(300, 300, text="Your score: " + str(self.stepcounter), fill='blue',
                                font='arial 18 bold')
        self.canvas.create_rectangle(200, 500, 400, 600, state="hidden")
        self.canvas.create_text(300, 550, text="Back to Menu", font="Arial 20 bold", activefill="red")

    def restart(self, e):

        if self.end:
            print("Restart")
            self.canvas.destroy()
            if __name__ == '__main__':
                p = Game(self.map, self.image_state, self.stepcounter)
                p.canvas.mainloop()

    def restart2(self, e):
        print("Restart2")
        self.canvas.destroy()
        if self.map == 'Actual_easy.txt':
            self.map = 'Board_easy.txt'
        if self.map == 'Actual_hard.txt':
            self.map = 'Board_hard.txt'
        if self.map == 'Actual_medium.txt':
            self.map = 'Board_medium.txt'
        if __name__ == '__main__':
            p = Game(self.map, self.image_state, 0)
            p.canvas.mainloop()

    def back(self, event):
        x = event.x
        y = event.y
        if self.end is not True:
            if (30 < x < 200) and (550 < y < 580):
                print("Back to menu")
                if self.map == 'Board_easy.txt':
                    self.save('Actual_easy.txt')
                    self.save_steps(self.stepcounter, 'Board_easy.txt')
                    print("Game saved")
                elif self.map == 'Board_medium.txt':
                    self.save('Actual_medium.txt')
                    self.save_steps(self.stepcounter, 'Board_medium.txt')
                    print("Game saved")
                elif self.map == 'Board_hard.txt':
                    self.save('Actual_hard.txt')
                    self.save_steps(self.stepcounter, 'Board_hard.txt')
                    print("Game saved")
                self.canvas.destroy()
                if __name__ == '__main__':
                    p = Intro()
                    p.canvas.mainloop()

        if self.end:
            if (200 < x < 400) and (400 < y < 600):
                print("Back to menu")
                self.canvas.destroy()
                if __name__ == '__main__':
                    p = Intro()
                    p.canvas.mainloop()

    def step_back(self, e):
        if self.limiter == 0:
            if self.recieve:
                self.box[self.boxindex].move(self.olda, self.oldb)
                self.object.move(self.oldx, self.oldy)
                self.stepcounter -= 1
                self.canvas.itemconfig(self.info, text="Number of steps: " + str(self.stepcounter))
                self.limiter = 1
            else:
                self.object.move(self.oldx, self.oldy)
                self.stepcounter -= 1
                self.canvas.itemconfig(self.info, text="Number of steps: " + str(self.stepcounter))
                self.limiter = 1

    def save(self, map):
        with open(map, 'w') as output2:
            for i in range(len(self.pole)):
                for j in range(len(self.pole[i])):
                    if j == self.object.x and i == self.object.y:
                        self.pole[i][j] = 'o'
                    for item in self.box:
                        if j == item.x and i == item.y:
                            self.pole[i][j] = '#'
                    output2.write(self.pole[i][j] + ' ')
                output2.write('\n')

                ###############################################################################################

    def save_steps(self, step, map):
        with open('numberofsteps.txt', 'w') as number:
            if map == 'Board_easy.txt':
                self.steppole[0] = self.stepcounter
            if map == 'Board_medium.txt':
                self.steppole[1] = self.stepcounter
            if map == 'Board_hard.txt':
                self.steppole[2] = self.stepcounter
            for item in self.steppole:
                number.write(str(item) + '\n')


##############################################################################################


class Animation:
    def __init__(self, canvas, dx):
        self.dx = dx
        self.canvas = canvas
        self.dynamic_state = False
        self.img1 = tkinter.PhotoImage(file='img/anim1.png')
        self.img2 = tkinter.PhotoImage(file='img/anim2.png')
        self.img3 = tkinter.PhotoImage(file='img/anim3.png')
        self.img4 = tkinter.PhotoImage(file='img/anim4.png')
        self.x1 = 50
        self.y1 = 580
        self.helper = 1
        self.animation = self.canvas.create_image(self.x1, self.y1, image=self.img1)
        self.control = False

    def call_state(self, state):
        self.dynamic_state = state
        return self.dynamic_state

    def movement(self):
        if self.control is not True:
            if self.dynamic_state == 1:
                self.canvas.itemconfig(self.animation, image=self.img1)
            else:
                self.canvas.itemconfig(self.animation, image=self.img4)
            self.x1 += self.dx
        if self.x1 >= 640:
            self.control = True
        if self.control and self.x1 >= -15:
            if self.dynamic_state == 1:
                self.canvas.itemconfig(self.animation, image=self.img2)
            else:
                self.canvas.itemconfig(self.animation, image=self.img3)
            self.x1 -= self.dx
        if self.x1 < -15:
            self.control = False
        self.canvas.coords(self.animation, self.x1, self.y1)


class Intro:
    def __init__(self):
        self.size = 30
        self.canvas = tkinter.Canvas(width=self.size * 20, height=self.size * 22)
        self.canvas.pack()
        self.background = tkinter.PhotoImage(file='img/bc_3.png')
        self.canvas.create_image(300, 330, image=self.background)
        self.message = self.canvas.create_text(400, 200, text="The choosen map is: Easy", font="Arial 18 bold")
        self.message2 = self.canvas.create_text(380, 450, text="Man selected!", font="Arial 15 bold")
        self.canvas.bind('<Button-1>', self.clicking)
        self.canvas.bind('<B1-Motion>', self.select)
        self.image_state = 0
        self.character1 = tkinter.PhotoImage(file='img/c1.png')
        self.character2 = tkinter.PhotoImage(file='img/c2.png')
        self.xx = 155
        self.yy = 225
        self.r = 15
        self.map = 'Board_easy.txt'
        self.load = False

        self.stepcounter = 0
        self.steppole = []

        self.draw_start()

        ########################################################################################################
        with open('numberofsteps.txt', 'r') as subor:
            for riadok in subor:
                riadok = int(riadok.strip())
                self.steppole.append(riadok)

        ########################################################################################################

        self.item = Animation(self.canvas, 5)

        self.timer()

    def timer(self):
        while True:
            self.item.movement()
            self.canvas.update()
            self.canvas.after(55)

    def draw_start(self):
        self.canvas.create_text(300, 80, text="Sokoban Game", fill="red", font="Arial 30 bold")
        self.canvas.create_text(120, 160, text="Choose a map:", fill="black", font="Arial 18 bold")

        self.canvas.create_rectangle(200, 500, 400, 600, state="hidden")
        self.canvas.create_text(300, 550, text="Play", font="Arial 32 bold", activefill="red")

        self.canvas.create_rectangle(0, 630, 610, 670, width="3")
        self.canvas.create_text(300, 645, text="Created by: Tóth Richárd", font="Arial 14 bold", activefill="red")

        self.canvas.create_line(155, 200, 155, 450, width=8)

        self.selector = self.canvas.create_oval(self.xx + self.r, self.yy + self.r, self.xx - self.r, self.yy - self.r,
                                                width=2, fill='red')

        self.canvas.create_text(80, 225, text="Easy", font="Arial 14 bold")
        self.canvas.create_text(80, 300, text="Medium", font="Arial 14 bold")
        self.canvas.create_text(80, 375, text="Hard", font="Arial 14 bold")

        self.button1 = self.canvas.create_rectangle(30, 470, 130, 510, fill="red")
        self.canvas.create_text(75, 490, text="Saved", font="Arial 17 bold", activefill="white")

        self.button2 = self.canvas.create_rectangle(130, 470, 220, 510, fill="green")
        self.canvas.create_text(175, 490, text="New", font="Arial 17 bold", activefill="white")

        self.canvas.create_text(400, 260, text="Select your player: ", font="Arial 18 bold")

        self.canvas.create_image(300, 380, image=self.character1)
        self.canvas.create_image(450, 380, image=self.character2)

    def clicking(self, event):
        x = event.x
        y = event.y
        if (200 < x < 400) and (450 < y < 600):
            print("Lets start!")
            self.canvas.destroy()
            if __name__ == '__main__':
                p = Game(self.map, self.image_state, self.stepcounter)  # MAIN GAME START
                p.canvas.mainloop()

        if (250 < x < 350) and (330 < y < 420):
            self.canvas.itemconfig(self.message2, text="Man selected!")
            self.image_state = 0
            print("Man selected!")

        if (400 < x < 500) and (330 < y < 420):
            self.image_state = 1
            self.canvas.itemconfig(self.message2, text="Snail selected!")
            print("Snail selected!")

        if (30 < x < 130) and (470 < y < 510) and self.load is not True:
            self.canvas.itemconfig(self.button1, fill="green")
            self.canvas.itemconfig(self.button2, fill="red")
            self.map = 'Actual_easy.txt'
            self.stepcounter = self.steppole[0]
            self.load = True
            self.canvas.coords(self.selector, 155 + 15, 225 + 15, 155 - 15, 225 - 15)
            print("Saved maps", self.load)

        if (130 < x < 220) and (470 < y < 520) and self.load is True:
            self.canvas.itemconfig(self.button2, fill="green")
            self.canvas.itemconfig(self.button1, fill="red")
            self.canvas.coords(self.selector, 155 + 15, 225 + 15, 155 - 15, 225 - 15)
            self.load = False
            print("New maps", self.load)

        self.item.call_state(self.image_state)

    def select(self, event):
        x = event.x
        y = event.y

        if (145 < x < 165) and 190 <= y <= 450:
            self.canvas.coords(self.selector, x + self.r, y + self.r, x - self.r, y - self.r)

            # Easy selector
        if (145 < x < 165) and (210 < y < 230) and self.load is not True:
            print("You have chosen the Easy map.")
            self.map = 'Board_easy.txt'
            self.canvas.itemconfig(self.message, text="The choosen map is: Easy")

        if (145 < x < 165) and (210 < y < 230) and self.load:
            print("You have loaded the  Saved Easy map.")
            self.map = 'Actual_easy.txt'
            self.stepcounter = self.steppole[0]
            self.canvas.itemconfig(self.message, text="The loaded map is: Easy")

            # Medium selector

        if (145 < x < 165) and (290 < y < 315) and self.load is not True:
            print("You have chosen the Medium map.")
            self.map = 'Board_medium.txt'
            self.canvas.itemconfig(self.message, text="The choosen map is: Medium")

        if (145 < x < 165) and (290 < y < 315) and self.load:
            print("You have loaded the Medium map.")
            self.map = 'Actual_medium.txt'
            self.stepcounter = self.steppole[1]
            self.canvas.itemconfig(self.message, text="The loaded map is: Medium")

            # Medium selector

        if (145 < x < 165) and (360 < y < 380) and self.load is not True:
            print("You have chosen the Hard map.")
            self.map = 'Board_hard.txt'
            self.canvas.itemconfig(self.message, text="The choosen map is: Hard")

        if (145 < x < 165) and (360 < y < 380) and self.load:
            print("You have chosen the Hard map.")
            self.map = 'Actual_hard.txt'
            self.stepcounter = self.steppole[2]
            self.canvas.itemconfig(self.message, text="The loaded map is: Hard")


if __name__ == '__main__':
    p = Intro()
    p.canvas.mainloop()
